terraform {
  # required_version = ">= 1.3.8"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }

  }
}



provider "aws" {
  #region = "us-east-1"
  #profile = "default"
}

terraform {
  backend "s3" {
    bucket         = "anslem-terraform-statefile-demo-2024-2"
    key            = "anslem-terraform/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "anslem-terraform-tf-state-lock"
  }
}




